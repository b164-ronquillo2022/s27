const http = require('http');

const port = 6000;

//Mock database
let directory = [
	{
                "firstName": "Mary Jane",
                "lastName": "Dela Cruz",
                "mobileNo": "09123456789",
                "email": "mjdelacruz@mail.com",
                "password": 123
        },
        {
                "firstName": "John",
                "lastName": "Doe",
                "mobileNo": "09123456789",
                "email": "jdoe@mail.com",
                "password": 123
        }
]

console.log(typeof directory)


const server = http.createServer((req, res) => {

	//route for returning ALL items upon receiving a GET request
	if(req.url == '/profile' && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end()
	}

	//create users
	if(req.url == '/profile' && req.method == "POST"){
		//Declare and initialize a 'requestBody' variable to an empty string
		//This will act as a placeholder for the resource/data to created later on
		let requestBody = '';


		//A "stream" is a sequence of data

		//data step - this is the 1st sequence of stream. this reads the "data" stream and process it as the request body.
		//the information provided from the request object(client) enters a sequence of "data" 
		req.on('data', function(data){
			//Assigns the data retrieved from the data stream to requestBody
			requestBody += data;

		});

		//response end step - only runs after the request has completely been sent
		req.on('end', function(){
			console.log(typeof requestBody);
			//Converts the string requestBody to JSON
		requestBody = JSON.parse(requestBody);

			//Create new object representing the new mock database record
			let newUser = {
				
                "firstName": requestBody.firstName,
                "lastName": requestBody.lastName,
                "mobileNo": requestBody.mobileNo,
                "email": requestBody.email,
                "password": requestBody.password
			}

			//Add the new user into the mock database
			directory.push(newUser);
			console.log(directory)

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end()
		})

	}


	});

    server.listen(port);

    console.log(`Server running at localhost:${port}`);